const express = require('express')
const proxy = require('express-http-proxy')
const bodyParser = require('body-parser')
const path = require('path')
const app = express()

// prettier-ignore
const {
  PORT = 8080,
  API_HOST = "localhost:15435"
} = process.env

app.use('/api', proxy(API_HOST))

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, 'client', 'build', 'index.html'))
})

app.use(express.static(path.join(__dirname, 'client', 'build')))

app.listen(PORT, () => {
  console.log(`Server listening on http://localhost:${PORT}...`)
})
