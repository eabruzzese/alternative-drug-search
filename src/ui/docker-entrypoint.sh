#!/usr/bin/env bash

CLIENT_DIR="/srv/client"

main () {
  [ $# -lt 1 ] && _run_server

  case "$1" in
    # If we're only passing flags, run the server with them.
    -*) _run_server "$@";;

    # If we're running another command, don't run the server.
    *) exec "$@";;
  esac
}

_run_server () {
  _install_dependencies
  _build_application

  exec yarn "$@"
}

_install_dependencies () {
  yarn install "$@"
  (
    cd "$CLIENT_DIR"
    yarn install "$@"
  )
}

_build_application () {
  if [[ ! -f "$CLIENT_DIR"/build/index.html ]]; then
    (
      cd "$CLIENT_DIR"

      # Perform the production build in the background so we don't block the container
      # from starting up (helpful when developing).
      yarn build &
    )
  fi
}

main "$@"
