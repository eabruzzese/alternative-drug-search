export const SET_SEARCH_TERM = 'search/SET_DRUG_SEARCH_TERM'
export const SET_LOADING = 'search/SET_LOADING'
export const REQUEST_DRUG_SEARCH = 'search/REQUEST_DRUG_SEARCH'
export const COMPLETE_DRUG_SEARCH = 'search/COMPLETE_DRUG_SEARCH'
export const REQUEST_ALTERNATIVES_SEARCH = 'search/REQUEST_ALTERNATIVES_SEARCH'
export const COMPLETE_ALTERNATIVES_SEARCH =
  'search/COMPLETE_ALTERNATIVES_SEARCH'
export const FETCH_POPULAR_SEARCHES = 'search/REQUEST_POPULAR_SEARCHES'
export const SET_ERROR = 'search/SET_ERROR'

const INITIAL_STATE = {
  searchTerm: '',
  loading: false,
  error: false,
  popularSearches: [],
  results: [],
  alternativesByRxcui: {},
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_POPULAR_SEARCHES:
      return {
        ...state,
        popularSearches: action.results,
      }

    case SET_SEARCH_TERM:
      return {
        ...state,
        searchTerm: action.searchTerm,
      }

    case SET_LOADING:
      return {
        ...state,
        loading: action.loading,
      }

    case REQUEST_DRUG_SEARCH:
      return {
        ...state,
        loading: true,
      }

    case COMPLETE_DRUG_SEARCH:
      return {
        ...state,
        loading: false,
        results: action.results,
      }

    case REQUEST_ALTERNATIVES_SEARCH:
      return {
        ...state,
        alternativesByRxcui: {
          ...state.alternativesByRxcui,
          [action.rxcui]: {
            loading: true,
          },
        },
      }

    case COMPLETE_ALTERNATIVES_SEARCH:
      return {
        ...state,
        alternativesByRxcui: {
          ...state.alternativesByRxcui,
          [action.rxcui]: {
            loading: false,
            hasMultipleIngredients: action.hasMultipleIngredients,
            results: action.results,
          },
        },
      }

    case SET_ERROR:
      return {
        ...state,
        error: action.error,
      }

    default:
      return state
  }
}

export const fetchPopularSearches = () => {
  return dispatch => {
    return fetch(encodeURI(`/api/v1/stats/popular-searches`))
      .then(res => res.json())
      .then(res => {
        dispatch({
          type: FETCH_POPULAR_SEARCHES,
          results: res.data,
        })
      })
      .catch(error => {
        dispatch({
          type: SET_ERROR,
          error,
        })
      })
  }
}

export const setSearchTerm = searchTerm => ({
  type: SET_SEARCH_TERM,
  searchTerm,
})

export const setLoading = loading => ({
  type: SET_LOADING,
  loading,
})

export const requestDrugSearch = () => {
  return (dispatch, getState) => {
    const {
      search: { searchTerm },
    } = getState()

    dispatch({
      type: REQUEST_DRUG_SEARCH,
    })

    return fetch(encodeURI(`/api/v1/drugs?name=${searchTerm}`))
      .then(res => res.json())
      .then(res => {
        dispatch({
          type: COMPLETE_DRUG_SEARCH,
          results: res.data,
        })
      })
      .catch(error => {
        dispatch({
          type: SET_ERROR,
          error,
        })
      })
  }
}

export const requestAlternativesSearch = rxcui => {
  return dispatch => {
    dispatch({
      type: REQUEST_ALTERNATIVES_SEARCH,
      rxcui,
    })

    return fetch(encodeURI(`/api/v1/drugs/${rxcui}/related`))
      .then(res => res.json())
      .then(res => {
        dispatch({
          type: COMPLETE_ALTERNATIVES_SEARCH,
          rxcui: rxcui,
          hasMultipleIngredients: res.meta.ingredients > 1,
          results: res.data,
        })
      })
      .catch(error => {
        dispatch({
          type: SET_ERROR,
          error,
        })
      })
  }
}
