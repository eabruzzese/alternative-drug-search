import DrugAlternatives from './DrugAlternatives'
import PopularSearches from './PopularSearches'
import SearchInterface from './SearchInterface'

export default {
  DrugAlternatives,
  PopularSearches,
  SearchInterface,
}
