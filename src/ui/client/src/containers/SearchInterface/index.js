import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { Grid, Loader } from 'semantic-ui-react'
import SearchForm from 'components/SearchForm'
import SearchResultsList from 'components/SearchResultsList'
import PopularSearches from 'containers/PopularSearches'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {
  setLoading,
  setSearchTerm,
  requestDrugSearch,
} from 'state/modules/search'
import debounce from 'lodash/debounce'

/**
 * SEARCH_DEBOUNCE_MS
 *
 * The `SEARCH_DEBOUNCE_MS` value specifies the minimum number of milliseconds to wait
 * after a keypress before firing an API call for a drug search.
 */
const SEARCH_DEBOUNCE_MS = 500

/**
 * SearchInterface
 *
 * The `SearchInterface` controls the layout and state of the components that make up
 * the drug search experience.
 */
class SearchInterface extends PureComponent {
  /**
   * _requestSearch
   *
   * Initiates a drug search based on the contents of the search box. Debounces requests
   * so that we're not searching on every keypress.
   */
  _requestSearch = debounce(searchTerm => {
    const { requestDrugSearch } = this.props
    requestDrugSearch()
  }, SEARCH_DEBOUNCE_MS)

  /**
   * _handleSearchTermChange
   *
   * Handles the state of the search box, and initiates drug searches if the search term
   * is non-empty.
   */
  _handleSearchTermChange = (e, { value: searchTerm }) => {
    const { setLoading, setSearchTerm } = this.props

    setSearchTerm(searchTerm)

    // Manually set the loading flag to prevent content flashes caused by the debounce.
    setLoading(!!searchTerm.length)

    if (searchTerm.length) {
      this._requestSearch(searchTerm)
    }
  }

  render() {
    const { searchTerm, loading, results } = this.props

    return (
      <Grid>
        <Grid.Row>
          <Grid.Column width={16}>
            <SearchForm
              searchTerm={searchTerm}
              onSearchTermChange={this._handleSearchTermChange}
            />
          </Grid.Column>
        </Grid.Row>

        {!searchTerm.length && (
          <Grid.Row>
            <Grid.Column width={16} textAlign="center">
              <PopularSearches />
            </Grid.Column>
          </Grid.Row>
        )}

        <Grid.Row>
          <Grid.Column width={16}>
            <Loader inline="centered" active={loading} size="huge">
              Searching for "{searchTerm}"...
            </Loader>

            {!loading && !!searchTerm.length && (
              <SearchResultsList searchTerm={searchTerm} results={results} />
            )}
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }
}

SearchInterface.propTypes = {
  loading: PropTypes.bool.isRequired,
  requestDrugSearch: PropTypes.func.isRequired,
  results: PropTypes.arrayOf(PropTypes.object).isRequired,
  searchTerm: PropTypes.string.isRequired,
  setLoading: PropTypes.func.isRequired,
  setSearchTerm: PropTypes.func.isRequired,
}

const mapStateToProps = ({ search }) => ({
  searchTerm: search.searchTerm,
  loading: search.loading,
  results: search.results,
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setLoading,
      setSearchTerm,
      requestDrugSearch,
    },
    dispatch,
  )

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SearchInterface)
