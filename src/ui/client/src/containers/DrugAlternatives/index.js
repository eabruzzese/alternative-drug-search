import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { Divider, Loader, Message, Segment } from 'semantic-ui-react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { requestAlternativesSearch } from 'state/modules/search'

/**
 * DisclaimerMessage
 *
 * The `DisclaimerMessage` informs the user of the non-official nature of the
 * alternatives listing.
 *
 * TODO: Externalize strings for i18n.
 */
const DisclaimerMessage = ({ ...rest }) => (
  <Message
    warning
    icon="warning sign"
    header="Disclaimer"
    content="The alternatives listed do not constitute medical advice or opinion. Talk to your physician before making changes to your medications."
    {...rest}
  />
)

/**
 * MultipleIngredientsMessage
 *
 * If a drug contains multiple ingredients, the `MultipleIngredientsMessage` will be
 * shown to let the user know that alternatives might only cover a subset of the
 * ingredients in the selected drug.
 *
 * TODO: Externalize strings for i18n.
 */
const MultipleIngredientsMessage = ({ ...rest }) => (
  <Message
    warning
    icon="warning sign"
    header="Warning: Multiple Ingredients"
    content="This drug contains multiple ingredients. The alternatives listed may only contain a subset of the ingredients in the original drug."
    {...rest}
  />
)

/**
 * DrugAlternatives
 *
 * The `DrugAlternatives` container displays alternatives to a drug based on a given
 * RXCUI.
 *
 * Takes an `rxcui` prop, which will be used to look up potential alternatives.
 *
 * TODO: Externalize strings for i18n.
 */
class DrugAlternatives extends PureComponent {
  /**
   * When the component mounts, we'll try to load alternative drugs for the given RXCUI.
   */
  componentDidMount = () => {
    this._loadAlternatives()
  }

  /**
   * Fetches alternatives for the given RXCUI.
   */
  _loadAlternatives = () => {
    const { requestAlternativesSearch, rxcui } = this.props
    requestAlternativesSearch(rxcui)
  }

  render() {
    const { loading, hasMultipleIngredients, results } = this.props

    return (
      <Segment>
        <Loader inline="centered" active={loading}>
          Finding potential alternatives...
        </Loader>
        {!loading && results && (
          <div>
            <DisclaimerMessage />
            <MultipleIngredientsMessage visible={hasMultipleIngredients} />
            <Divider horizontal section>
              {results.length} Potential Alternatives
            </Divider>
            {results.map(result => (
              <Segment key={result.rxcui} vertical>
                {result.name}
              </Segment>
            ))}
          </div>
        )}
      </Segment>
    )
  }
}

DrugAlternatives.propTypes = {
  hasMultipleIngredients: PropTypes.bool,
  loading: PropTypes.bool,
  requestAlternativesSearch: PropTypes.func.isRequired,
  results: PropTypes.arrayOf(PropTypes.object),
  rxcui: PropTypes.string.isRequired,
}

const mapStateToProps = ({ search: { alternativesByRxcui } }, { rxcui }) => {
  const alternative = alternativesByRxcui[rxcui] || {}
  const hasEverLoaded = alternative !== {}

  return {
    hasMultipleIngredients: hasEverLoaded && alternative.hasMultipleIngredients,
    loading: hasEverLoaded && alternative.loading,
    results: (hasEverLoaded && alternative.results) || [],
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      requestAlternativesSearch,
    },
    dispatch,
  )

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DrugAlternatives)
