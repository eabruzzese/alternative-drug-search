import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Label } from 'semantic-ui-react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {
  fetchPopularSearches,
  setSearchTerm,
  requestDrugSearch,
} from 'state/modules/search'

/**
 * ClickableLabel
 *
 * A styled version of the Semantic UI `Label` component to better indicate that it's
 * clickable.
 */
const ClickableLabel = styled(Label)`
  background: inherit;
  transition: background 0.25s;

  &:hover {
    background: #21ba45;
    color: #ffffff;
    cursor: pointer;
    transition: background 0.25s, color 0.25s;
  }
`

/**
 * PopularSearches
 *
 * The `PopularSearches` container loads the most popular searches made against the
 * service and presents them as clickable labels. Clicking a label sets the search term
 * and executes a drug for that search.
 */
class PopularSearches extends PureComponent {
  /**
   * componentDidMount
   *
   * When the component mounts, popular searches will be loaded from the API.
   */
  componentDidMount() {
    const { fetchPopularSearches } = this.props
    fetchPopularSearches()
  }

  /**
   * _handleClick
   *
   * When a user clicks on a popular search, the search term is set and a search is
   * requested immediately.
   */
  _handleClick = searchTerm => {
    const { setSearchTerm, requestDrugSearch } = this.props
    setSearchTerm(searchTerm)
    requestDrugSearch()
  }

  render() {
    const { popularSearches } = this.props

    return (
      popularSearches && (
        <Label.Group size="large">
          {popularSearches.map(term => (
            <ClickableLabel key={term} onClick={() => this._handleClick(term)}>
              {term}
            </ClickableLabel>
          ))}
        </Label.Group>
      )
    )
  }
}

PopularSearches.propTypes = {
  fetchPopularSearches: PropTypes.func.isRequired,
  popularSearches: PropTypes.arrayOf(PropTypes.string),
  requestDrugSearch: PropTypes.func.isRequired,
  setSearchTerm: PropTypes.func.isRequired,
}

const mapStateToProps = ({ search: { popularSearches } }) => ({
  popularSearches,
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      fetchPopularSearches,
      setSearchTerm,
      requestDrugSearch,
    },
    dispatch,
  )

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PopularSearches)
