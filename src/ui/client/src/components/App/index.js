import React from 'react'
import { Divider } from 'semantic-ui-react'
import Header from 'components/Header'
import Layout from 'components/Layout'
import SearchInterface from 'containers/SearchInterface'
import { Provider } from 'react-redux'
import store from 'state'

/**
 * App
 *
 * The root component for the application. Exposes a simple header and a search
 * interface.
 */
const App = () => (
  <Provider store={store}>
    <Layout verticalAlign="middle">
      <Header />
      <Divider hidden />
      <SearchInterface />
    </Layout>
  </Provider>
)

export default App
