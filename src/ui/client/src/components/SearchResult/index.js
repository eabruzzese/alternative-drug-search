import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Grid, Header, Label, Icon, Segment } from 'semantic-ui-react'
import Highlighter from 'react-highlight-words'
import DrugAlternatives from 'containers/DrugAlternatives'

/**
 * TTY_MAP
 *
 * The TTY_MAP is a simple mapping of RxNorm TTY code to a layman-friendly label.
 *
 * TODO: Externalize strings for i18n.
 */
const TTY_MAP = {
  SBD: 'Brand',
  SCD: 'Generic',
}

/**
 * Highlight
 *
 * A custom highlight style used for highlighting the search term within search results.
 */
const Highlight = styled.mark`
  background: #fffb9e;
  padding: 0.1em 0.25em 0.2em 0.25em;
  border-radius: 0.25em;
  border-bottom: 2px dotted #e8e233;
`

/**
 * ClickableSegment
 *
 * A styled version of Semantic UI's Segment component that makes it appear clickable.
 */
const ClickableSegment = styled(Segment)`
  background: inherit;
  transition: background 0.25s;

  &:hover {
    background: #f9f9f9;
    cursor: pointer;
    transition: background 0.25s;
  }
`

/**
 * CollapseIndicator
 *
 * A chevron icon that rotates based on its `collapsed` prop.
 */
const CollapseIndicator = ({ collapsed }) => (
  <Icon
    style={{ height: '100%', padding: '0.5em' }}
    name={`chevron ${collapsed ? 'down' : 'right'}`}
    size="large"
  />
)

CollapseIndicator.propTypes = {
  collapsed: PropTypes.bool.isRequired,
}

/**
 * SearchResult
 *
 * An entry in the list of search results. When clicked, toggles the display of a list
 * of (potentially) alternative drugs.
 */
class SearchResult extends PureComponent {
  state = {
    alternativesVisible: false, // If true, displays the list of alternative drugs.
  }

  /**
   * _handleClick
   *
   * Toggles the display of the alternative drugs list.
   */
  _handleClick = () => {
    const { alternativesVisible } = this.state
    this.setState({
      alternativesVisible: !alternativesVisible,
    })
  }

  render() {
    const { alternativesVisible } = this.state
    const { result, highlight = '' } = this.props

    return (
      <Segment.Group>
        <ClickableSegment clearing onClick={this._handleClick}>
          <Grid columns={16}>
            <Grid.Column width={15}>
              <Header as="h3" floated="left">
                <Highlighter
                  highlightTag={Highlight}
                  searchWords={[highlight]}
                  textToHighlight={result.name}
                />
              </Header>
              <Label.Group>
                <Label>RXCUI {result.rxcui}</Label>
                <Label>{TTY_MAP[result.tty]}</Label>
              </Label.Group>
            </Grid.Column>
            <Grid.Column width={1} textAlign="right">
              <CollapseIndicator collapsed={alternativesVisible} />
            </Grid.Column>
          </Grid>
        </ClickableSegment>
        {alternativesVisible && (
          <DrugAlternatives attached="bottom" rxcui={result.rxcui} />
        )}
      </Segment.Group>
    )
  }
}

SearchResult.propTypes = {
  result: PropTypes.shape({
    name: PropTypes.string.isRequired,
    rxcui: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    tty: PropTypes.string.isRequired,
  }),
}

export default SearchResult
