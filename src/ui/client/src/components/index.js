import App from "./App"
import Header from './Header'
import Layout from './Layout'
import SearchForm from './SearchForm'
import SearchResult from './SearchResult'
import SearchResultsList from './SearchResultsList'

export default {
  App,
  Header,
  Layout,
  SearchForm,
  SearchResult,
  SearchResultsList,
}
