import React from 'react'
import PropTypes from 'prop-types'
import { Message } from 'semantic-ui-react'
import SearchResult from 'components/SearchResult'

/**
 * NoResultsMessage
 *
 * A warning-like message notifying the user that no results were found for their
 * search.
 */
const NoResultsMessage = ({ searchTerm }) => (
  <Message
    warning
    icon="warning circle"
    header={`No drugs were found matching "${searchTerm}"`}
    content="Maybe the spelling's wrong? Double-check and try again."
  />
)

NoResultsMessage.propTypes = {
  resultCount: PropTypes.number.isRequired,
  searchTerm: PropTypes.string.isRequired,
}

/**
 * ResultsMessage
 *
 * A message summarizing the number of results found.
 */
const ResultsMessage = ({ resultCount, searchTerm }) => (
  <Message
    success
    icon="pills"
    header={`Found ${resultCount} drugs found matching "${searchTerm}"`}
    content="Click on a result to see alternative therapies."
  />
)

ResultsMessage.propTypes = {
  resultCount: PropTypes.number.isRequired,
  searchTerm: PropTypes.string.isRequired,
}

/**
 * SearchResults
 *
 * If any results were found for the user's query, displays a message and a list of
 * results.
 *
 * If no results were returned, a "no results" message is displayed.
 */
const SearchResults = ({ searchTerm, results }) =>
  (results.length && (
    <div>
      <ResultsMessage resultCount={results.length} searchTerm={searchTerm} />
      {results.map(result => (
        <SearchResult
          key={result.rxcui}
          result={result}
          highlight={searchTerm}
        />
      ))}
    </div>
  )) || <NoResultsMessage searchTerm={searchTerm} />

SearchResults.propTypes = {
  searchTerm: PropTypes.string.isRequired,
  results: PropTypes.arrayOf(PropTypes.object).isRequired,
}

export default SearchResults
