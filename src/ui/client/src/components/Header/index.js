import React from 'react'
import { Header, Icon } from 'semantic-ui-react'

const CustomHeader = () => (
  <Header as="h1" textAlign="center">
    <Header.Content>
      <Icon name="search" />
      Drug Alternatives Search
    </Header.Content>
    <Header.Subheader>
      Search for alternatives to your drugs.
    </Header.Subheader>
  </Header>
)

export default CustomHeader
