import React from 'react'
import PropTypes from 'prop-types'
import { Form, Input } from 'semantic-ui-react'

/**
 * SearchForm
 *
 * Displays a for with an input field for searching for drugs by name. The form state is
 * managed by the parent(s).
 *
 * TODO: Externalize strings for i18n.
 */
const SearchForm = ({ searchTerm, onSearchTermChange }) => (
  <Form>
    <Form.Field>
      <Input
        autoFocus
        value={searchTerm}
        onChange={onSearchTermChange}
        placeholder="Search for a drug by name"
        icon="search"
        size="massive"
      />
    </Form.Field>
  </Form>
)

SearchForm.propTypes = {
  searchTerm: PropTypes.string.isRequired,
  onSearchTermChange: PropTypes.func.isRequired,
}

export default SearchForm
