import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Grid } from 'semantic-ui-react'

/**
 * FullHeightGrid
 *
 * A styled version of Semantic UI's `Grid` component that streteches to the full
 * viewport height, and introduces some padding for viewing comfort.
 */
const FullHeightGrid = styled(Grid)`
  height: 100vh;
  padding: 2em 0 !important;
`

/**
 * Layout
 *
 * Provides a centered, full-height grid with padding. Adjusts based on the client's
 * viewport size.
 */
const Layout = ({ children, ...rest }) => (
  <FullHeightGrid centered padded {...rest}>
    <Grid.Column computer={12} tablet={14} mobile={16}>
      {children}
    </Grid.Column>
  </FullHeightGrid>
)

Layout.propTypes = {
  children: PropTypes.arrayOf(PropTypes.element).isRequired,
}

export default Layout
