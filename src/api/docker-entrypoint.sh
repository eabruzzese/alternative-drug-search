#!/bin/sh

main() {
  [ $# -lt 1 ] && _run_server

  case "$1" in
    # If we're only passing flags, run the server with them.
    -*) _run_server "$@";;

    # If we're running another command, don't run the server.
    *) exec "$@";;
  esac
}

_run_server() {
  _setup_database
  exec gunicorn autoapp:app "$@"
}

_setup_database () {
  flask db upgrade
  flask seed
}

main "$@"
