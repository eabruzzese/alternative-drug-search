"""models

Models for drug-related concepts in the application.
"""

from rxnorm_search.database import Model, Column, db


class DrugSearch(Model):
    """DrugSearch

    A model for storing historical searches from users. Useful for analytics or
    surfacing search suggestions.
    """

    __tablename__ = "drug_searches"

    search_type = Column(db.String, index=True)
    search_term = Column(db.String)
    result_count = Column(db.Integer, index=True)
