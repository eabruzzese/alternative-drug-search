"""endpoints

Provides endpoints for proxying queries to the RxNorm API.
"""

from typing import Tuple, Union

from rxnorm_search.repositories import StatsRepository


def get_popular_searches(limit: int) -> Union[dict, Tuple[dict, int]]:
    """get_popular_searches

    Fetches a list of the most popular searches against the service.

    :param limit:
        The number of popular searches to return.
    """
    stats = StatsRepository()
    popular_searches = stats.find_popular_searches()

    return popular_searches
