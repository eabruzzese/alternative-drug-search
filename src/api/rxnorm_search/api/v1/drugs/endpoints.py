"""endpoints

Provides endpoints for proxying queries to the RxNorm API.
"""

from typing import Tuple, Union

from rxnorm_search.extensions import cache
from rxnorm_search.models import DrugSearch
from rxnorm_search.repositories import RxNormRepository, StatsRepository


@cache.memoize(60)
def search(name: str) -> Union[dict, Tuple[dict, int]]:
    """search

    Given a drug name, searches the RxNorm API for that drug.

    :param name:
        The string name of the drug.
    """
    rxnorm = RxNormRepository()
    stats = StatsRepository()

    drugs = rxnorm.suggest_drugs_by_name(name=name)

    stats.record_drug_search(
        search_term=name,
        result_count=len(drugs["data"])
    )

    return drugs


@cache.memoize(60)
def search_related(rxcui: str) -> Union[dict, Tuple[dict, int]]:
    """search_related

    Given an RXCUI for a drug, searches the RxNorm API for related drugs.

    :param rxcui:
        The RXCUI of the drug in RxNorm.
    """
    rxnorm = RxNormRepository()
    stats = StatsRepository()

    related_drugs = rxnorm.find_related_drugs_by_rxcui(rxcui=rxcui)

    stats.record_alternatives_search(
        rxcui=rxcui,
        result_count=len(related_drugs["data"])
    )

    return related_drugs
