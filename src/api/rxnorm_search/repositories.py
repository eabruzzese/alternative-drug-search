"""repositories

Repositories responsible for sourcing and aggregating drug-related data from the RxNorm
API.
"""

import urllib

import jmespath
import requests

from typing import Iterable

from flask import current_app as app

from rxnorm_search.database import db
from rxnorm_search.models import DrugSearch


class RxNormRepository:
    """RxNormRepository

    Fetches drug data from the RxNorm API based on user input.
    """

    def __init__(self, rxnorm_api_base=None):
        self._rxnorm_api_base = rxnorm_api_base or app.config.get("RXNORM_API_BASE")

    def suggest_drugs_by_name(self, name: str) -> Iterable[dict]:
        """suggest_drugs_by_name

        Given a drug name, queries for related drug concepts in the RxNorm API.

        :param name:
            The name of the drug for which to find concepts.
        """
        response_json = self._query_rxnorm("drugs.json", params={"name": name})

        # Extract only the drug concepts from the result using JMESPath. We only
        # want entries that include ingredient, strength, dose form, and
        # (optionally) the brand name (SCD and SBD TTYs).
        concepts = (
            jmespath.search(
                "drugGroup"
                ".conceptGroup[?tty == `SCD` || tty == `SBD`]"
                ".conceptProperties | []",
                response_json,
            )
            or []
        )

        # Sort for easier skimming.
        concepts = sorted(concepts, key=lambda c: c["name"])

        return {"data": concepts, "meta": {}}

    def find_related_drugs_by_rxcui(self, rxcui: int) -> Iterable[dict]:
        """find_related_drugs_by_rxcui

        Given the RXCUI of a drug in RxNorm, finds related drugs.

        :param rxcui:
            The RXCUI of the subject drug in RxNorm.
        """
        # Find drug ingredients, and collect the RXCUIs from the response.
        response_json = self._query_rxnorm(
            f"rxcui/{rxcui}/related.json", params={"tty": "IN"}
        )

        # fmt: off
        # Collect RXCUIs from the ingredient response.
        ingredient_rxcuis = jmespath.search(
            "relatedGroup"
            ".conceptGroup[]"
            ".conceptProperties[]"
            ".rxcui",
            response_json,
        )
        # fmt: on

        alternative_drugs = []
        for ingredient_rxcui in ingredient_rxcuis:
            response_json = self._query_rxnorm(
                f"rxcui/{ingredient_rxcui}/related.json",
                # We have to use a string for params here, as dict-based params
                # are urlencoded, which the RxNorm API doesn't play well with.
                params="tty=SCD+SBD",
            )

            # fmt: off
            alternatives_for_ingredient = jmespath.search(
                "relatedGroup"
                ".conceptGroup[]"
                ".conceptProperties | []",
                response_json,
            )
            # fmt: on

            alternative_drugs.extend(alternatives_for_ingredient)

        # Sort for easier skimming.
        alternative_drugs = sorted(alternative_drugs, key=lambda d: d["name"])

        return {
            "data": alternative_drugs,
            "meta": {"ingredients": len(ingredient_rxcuis)},
        }

    def _query_rxnorm(self, path, *args, **kwargs):
        """_query_rxnorm

        Queries the RxNorm API with the given path. Passes args and kwargs to the
        `requests.get` call.

        :param path:
            The path to query in the API.
        """
        url = f"{self._rxnorm_api_base}/{path}"
        res = requests.get(url, **kwargs)
        res.raise_for_status()

        return res.json()


class StatsRepository:
    """StatsRepository

    A repository for aggregating service statistics.
    """

    QUERY_POPULAR_SEARCHES = """
        SELECT
            search_term
        FROM (
            SELECT
              LOWER(TRIM(REGEXP_REPLACE(search_term, '\s+', ' ', 'g'))) AS search_term
            FROM drug_searches
            WHERE result_count > 0
              AND search_type = 'BY_DRUG_NAME'
        ) cleaned_searches
        GROUP BY search_term
        ORDER BY COUNT(*) DESC
        LIMIT :limit
    """

    def find_popular_searches(self, limit=None):
        """find_popular_searches

        Fetches a list of the most popular searches, based on frequency.

        :param limit:
            The number of popular searches to return.
        """
        limit = limit or 5
        query = db.text(self.QUERY_POPULAR_SEARCHES)
        results = db.session.execute(query, params={"limit": limit}).fetchall()

        return {"data": [r[0] for r in results], "meta": {}}

    def record_drug_search(self, search_term: str, result_count: int):
        """record_drug_search

        Creates a new drug search record when searching for a drug by name.

        :param search_term:
            The term searched for.
        :param result_count:
            The number of results returned for the search.
        """
        return DrugSearch.create(
            search_type="BY_DRUG_NAME",
            search_term=search_term,
            result_count=result_count,
        )

    def record_alternatives_search(self, rxcui: str, result_count: int):
        """record_alternatives_search

        Creates a new drug search record when searching for drug alternatives.
        :param search_term:
            The RXCUI searched for.
        :param result_count:
            The number of results returned for the search.
        """
        return DrugSearch.create(
            search_type="BY_RELATED_RXCUI",
            search_term=rxcui,
            result_count=result_count,
        )
