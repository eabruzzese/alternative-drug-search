# -*- coding: utf-8 -*-
""" Database seeding. """

import click

from flask.cli import with_appcontext

from rxnorm_search.models import DrugSearch


INITIAL_SEARCHES = [
    "alavert",
    "ibuprofen",
    "caduet",
    "vyvanse",
    "erlotinib"
]


@click.command()
@with_appcontext
def seed():
    """seed

    Seeds the application with initial data.
    """
    click.echo("Seeding initial data...")

    for term in INITIAL_SEARCHES:
        DrugSearch.create(
            search_type="BY_DRUG_NAME",
            search_term=term,
            result_count=1
        )

    click.secho("Done!", fg="green")
