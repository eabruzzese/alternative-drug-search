#!/usr/bin/env python

"""setup

Setup the rxnorm_search module.
"""

from pathlib import Path
from setuptools import setup
from shutil import which

# Only use the SCM version if we have a functional git environment.
git_exists = Path(".git").exists() and which("git")

if git_exists:
    setup(use_scm_version=True)
else:
    setup()
