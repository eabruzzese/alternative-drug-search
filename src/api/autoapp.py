"""autoapp

Creates an application instance. Primarily used with wgsi managers like Gunicorn to
provide a succinct way to run the application.
"""

from rxnorm_search.app import create_app  # noqa
from rxnorm_search.settings import Config

app = create_app(config_object=Config)
