# Alternative Drug Search

The Alternative Drug Search is a web-based application for finding drug alternatives. A
user would use this service by searching for a drug and clicking on the most relevant
result to view its potential alternatives.

>**⚠️  Disclaimer:** Obviously this is just a toy application, and none of the information
>that it surfaces should be taken as medical advice or opinion.

## Demo

You can see a live demo here: http://alternative-drug-search.ericabruzzese.com:8080

## Setting Up

If you'd like to set up locally and poke around, it's pretty easy! You'll need:

* **Docker** (>= 18)
* **docker-compose** (>= 1.23)

Once you have them, you can simply:

```shell
$ bin/setup
```

Once you're up and running, the setup script will output the URLs for the accessible
services. If you missed it, you can run `bin/info` to see those URLs again. By default,
they're:

* API: http://localhost:5000/v1/
* API (Swagger): http://localhost:5000/v1/ui/
* UI: http://localhost:3000/

## Development Scripts

Several helper scripts are available in the `bin/` directory. They're mostly thin
wrappers around `docker-compose` commands to make working in a Docker workflow a little
easier:

* `bin/info` will output the details of where services are running.
* `bin/logs` will tail the logs of the docker-compose stack. Any arguments will be
  passed to the underlying `docker-compose logs -f`.
* `bin/restart` will restart the docker-compose stack. Any arguments will be passed to
  the underlying `docker-compose restart` command. (Hint: pass `api` or `ui` to restart
  a single service)
* `bin/setup` will set the development environment up from scratch.
* `bin/start` will start the docker-compose stack (if it's stopped).
* `bin/stop` will stop the docker-compose stack (if it's started).

## Architecture

The sections that follow will go over the general architecture of the application.

### Overview

The scope of the application is fairly narrow: a user uses the UI to search for a drug,
and clicks on any of the returned results to view potential alternatives for it.

In terms of data flow:

1. A user uses the React-based UI to search for a drug.
1. The search request is proxied from the UI host (a minimal NodeJS backend) to the API
   service.
1. The API service makes the appropriate API call(s) to the RxNorm API.
1. The results of the RxNorm call(s) are marshalled, cached, and returned to the UI
   service.
1. The React UI displays the results to the user.

Let's take a look at how these services are composed.

### Service Composition

The application contains two primary services: an `api` service, and a `ui` service.
You'll find each of them contained in their own directory under `src`. Additionally, it
makes use of a vanilla `postgres` container for data persistence.

Each service contains its own `Dockerfile` to allow it to be built as part of the
composition defined in `docker-compose.yml`.

In the next few sections, we'll dig into each of the primary services.

### The UI service (`src/ui`)

The UI service is a small NodeJS server that serves a React application. The React
application makes calls to the NodeJS backend, which proxies most requests to the API
service.

#### The NodeJS Server

The UI is served by a very small NodeJS-based server. Its job is to serve the static
assets and to proxy API requests to the backend API service.

All of the code for the server is contained in `src/ui/server.js`.

#### The React Application

The React application that the UI service provides is a simple `create-react-app`-based
app with the addition of Redux and a small number of dependencies.

Given the small scope of the application, the React app's code structure
in `src/ui/client/src` is a pretty basic combination of components, containers, and
Redux state:

* The `components/` directory contains (mostly) stateless components for controlling
  display.
* The `containers/` directory contains components that manage application state.
* The `state/` directory contains the Redux state for the application, split into
  modules (only one, for now).

For more information, read the source code in `src/ui`.

### The API service (`src/api`)

The `api` service (found in `src/api`) acts as a middleman between the `ui` service and
the RxNorm API. It's a Python 3 + Flask application that takes requests to its
`/v1/drugs` API endpoints and makes appropriate API calls to the RxNorm REST API. Its
purpose is to marshal RxNorm responses, provide caching, and collect search information
to power the "popular searches" feature in the UI.

#### The Stack

* Alembic ([docs](http://alembic.zzzcomputing.com/en/latest/)) for database migrations.
* Connexion ([docs](https://connexion.readthedocs.io/en/latest/)) for Swagger REST API
  support.
* Docker ([docs](https://docs.docker.com/)) for containerization.
* Flask ([docs](http://flask.pocoo.org/docs/)) for the application context.
* Flask-Cache ([docs](https://pythonhosted.org/Flask-Cache/)) for caching and
  memoization.
* Flask-Marshmallow ([docs](https://flask-marshmallow.readthedocs.io/en/latest/)) for
  response marshalling.
* Gunicorn ([docs](http://docs.gunicorn.org/en/stable/settings.html)) for wsgi.
* Python 3.7
* SQLAlchemy ([docs](http://docs.sqlalchemy.org/en/latest/)) for data persistence to
  PostgreSQL.

#### Code Structure

The code structure follows a common Python module pattern.

The root of the service (`src/api`) contains a `setup.cfg` and a `setup.py` file, which
are used to define and install the Flask application as a Python module called
`rxnorm_search`.

The root also contains `migrations/` (database migrations), `tests/` (the Pytest suite)
and a few files for configuring the WSGI runner (autoapp.py, structlogger.py) and the
Docker-related code.

The actual application code is under the `rxnorm_search/` directory, structured as a
fairly typical (mid-sized) Flask application.

(TODO: Provide more details on the application code)

#### Available Endpoints

The API service exposes several endpoints:

* The `/v1/drugs` endpoint takes a `name` query parameter. It returns drug objects from
  the upstream RxNorm API.
* The `/v1/drugs/{rxcui}/related` endpoint returns drugs that are related to the drug
  with the specified RXCUI.
* The `/v1/health/alive` and `/v1/health/ready` are for monitoring service health (used
  with container orchestration platforms like Docker Swarm or Kubernetes).

>**Note:** You'll notice that all of the above endpoints are prefixed with `/v1`. The
API service is built with evolution in mind, and can be easily extended with another
API version.

For more information, read the source code in `src/api`.
